This is a simple example of how to use React and Bootstrap together WITHOUT 
using node.js. The backend is run with Flask, and this will need to be 
configured to source the correct directories for source files.

I was inspired to do this project from the following youtube video, and I 
recommend watching it before getting started.

https://www.youtube.com/watch?v=1iAG6h9ff5s

You can find the bootstrap template I used for this exercise at the following 
link below.

https://startbootstrap.com/template-overviews/sb-admin/

One of the big caveats was getting react-router to work. I found a version that 
brought it all together, which is listed below. Props to whoever is keeping this
up to date.

http://www.bootcdn.cn/react-router/

This is code I will maintain at my leisure - but give to others who may be 
looking for a working example using React and Bootstrap to build a single page 
web app. Feel free to branch and create your own custom setups. Good luck my 
friends!


-S