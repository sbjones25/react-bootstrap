
import sys, os
from flask import Flask, render_template

from flask.ext.login import LoginManager, UserMixin
# from routes import *


#Set Directory Structure based on location
STATIC_DIR = '/path/to/static/dir/on/live/server/'
TEMPLATE_DIR = '/path/to/template/dir/on/live/server/'

if os.getenv("USER",None) == 'YOUR USERNAME':
    STATIC_DIR = '/path/to/local/static/dir'
    TEMPLATE_DIR = '/path/to/local/temp/dir'

app = Flask(__name__,
            static_folder=STATIC_DIR,
            template_folder=TEMPLATE_DIR)

app.config['TEMPLATES_AUTO_RELOAD'] = True


@app.route('/')
def index():
    return render_template('square.html')


#Set up routing system
# app.register_blueprint(routes)

if __name__ == '__main__':
    app.run()
