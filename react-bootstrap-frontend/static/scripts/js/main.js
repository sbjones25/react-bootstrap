require.config({
  baseUrl: "/static/scripts/js/",

  paths: {
    'jquery': '//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min',
    'bootstrap': '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min',
    'react': '//cdnjs.cloudflare.com/ajax/libs/react/15.2.1/react-with-addons',
    "JSXTransformer": "/static/scripts/js/JSXTransformer",
    'react-dom': '//cdnjs.cloudflare.com/ajax/libs/react/15.2.0/react-dom',
    // 'react-router': '//cdnjs.cloudflare.com/ajax/libs/react-router/1.0.0/ReactRouter',
    'react-router' : '//cdn.bootcss.com/react-router/3.0.0-alpha.3/ReactRouter',
    'react-bootstrap': '//cdnjs.cloudflare.com/ajax/libs/react-bootstrap/0.30.0/react-bootstrap',
  },

  jsx: {
    fileExtension: '.jsx',
    harmony: true,
    stripTypes: true
  }
});

require([
    'jquery',
    'bootstrap',
    'react', 
    'react-dom',
    '/static/scripts/js/jsx!components/MyRouter','startup'], 


function(jQuery, Bootstrap, React, ReactDOM, MyRouter) {
  
  App = React.createFactory(MyRouter);
  console.log("Starting Application")

  // Mount the JSX component in the app container

  ReactDOM.render(
      App(),
      document.getElementById('react-root'));

});


