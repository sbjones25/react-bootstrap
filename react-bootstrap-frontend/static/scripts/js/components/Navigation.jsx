define(['react','react-router'], function(React, ReactRouter) {

    var Link = ReactRouter.Link;

var NavbarHeader = React.createClass({
    render: function() {
        return (
            <div className="navbar-header">
                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                </button>
                <Link to='/' className="navbar-brand" href="/"><h4>Square</h4></Link>
            </div>
            )
    },
});

var NavbarRight = React.createClass({
    render: function() {
        return (
            <ul className="nav navbar-right top-nav">
                <li className="dropdown">
                    <a href="#" className="dropdown-toggle" data-toggle="dropdown"><i className="fa fa-user"></i> Welcome <b className="caret"></b></a>
                    <ul className="dropdown-menu">
                        <li>
                            <Link to="/profile"><i className="fa fa-fw fa-user"></i> Profile</Link>
                        </li>
                        <li>
                            <Link to="/settings"><i className="fa fa-fw fa-gear"></i> Settings</Link>
                        </li>
                        <li className="divider"></li>
                        <li>
                            <Link to="/login"><i className="fa fa-fw fa-power-off"></i> Log In</Link>
                        </li>
                    </ul>
                </li>
            </ul>
            )
    },
});


var NavbarCollapse = React.createClass({
    render: function() {
        return (
            <div className="collapse navbar-collapse navbar-ex1-collapse">
                <ul className="nav navbar-nav side-nav">
                    <li className="active">
                        <Link to="dashboard" ><i className="fa fa-fw fa-dashboard"></i> Dashboard</Link>
                    </li>
                    <li>
                        <Link to="charts"><i className="fa fa-fw fa-bar-chart-o"></i> Charts </Link>
                    </li>
                    <li>
                        <Link to="tables"><i className="fa fa-fw fa-table"></i> Tables</Link>
                    </li>
                    <li>
                        <a href="bootstrap-grid.html"><i className="fa fa-fw fa-wrench"></i> Bootstrap Grid</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i className="fa fa-fw fa-arrows-v"></i> Dropdown <i className="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" className="collapse">
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="blank-page.html"><i className="fa fa-fw fa-file"></i> Blank Page</a>
                    </li>
                    <li>
                        <a href="index-rtl.html"><i className="fa fa-fw fa-dashboard"></i> RTL Dashboard</a>
                    </li>
                </ul>
            </div>
            );
    },
});

var Navigation = React.createClass({
    render: function() {
        console.log('Navigation')
        return (
            <nav className="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div id="wrapper">
                    <nav className="navbar navbar-inverse navbar-fixed-top" role="navigation">
                        <NavbarHeader />
                        <NavbarRight />
                        <NavbarCollapse />
                    </nav>
                </div>
            </nav>
            )
    },
});

return Navigation;

});