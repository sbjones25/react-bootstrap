define(['react','react-router','/static/scripts/js/jsx!components/MainPage',
        '/static/scripts/js/jsx!components/Navigation',
        '/static/scripts/js/jsx!components/Dashboard',
        '/static/scripts/js/jsx!components/Content',
        '/static/scripts/js/jsx!components/Charts'], function(React,ReactRouter,MainPage, Nav, Dash, Cont, Chart) {
    
    var Router = ReactRouter.Router;
    var Route = ReactRouter.Route;
    var IndexRoute = ReactRouter.IndexRoute;
    var browserHistory = ReactRouter.browserHistory;
    var Navigation = Nav;
    var Dashboard = Dash;
    var Content = Cont;
    var Charts = Chart;

    var MyRouter = React.createClass({
        render: function(){
            return(
                <Router history={browserHistory}>
                    <Route component={MainPage}>
                        <Route path='/' component={Content} />
                        <Route path='/dashboard' component={Dashboard} />
                        <Route path='/charts' component={Charts} />
                    </Route>
                </Router>
            );
        },
    });

    return MyRouter;

});
