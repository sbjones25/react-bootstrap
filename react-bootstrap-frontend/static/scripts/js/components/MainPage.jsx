define(['react', 'bootstrap','react-router',
        '/static/scripts/js/jsx!components/Navigation',
        '/static/scripts/js/jsx!components/Dashboard',
        '/static/scripts/js/jsx!components/Content'], function(
                                                              React, Bootstrap, ReactRouter, 
                                                              Nav, Dash, Cont) {

  // var Content = require('./Content')

  console.log(ReactRouter)
  var Navigation = Nav;
  var Dashboard = Dash;
  var Content = Cont;
  var Router = ReactRouter.Router;
  var Route = ReactRouter.Route;
  var browserHistory = ReactRouter.browserHistory;

  // var IndexRoute = ReactRouter.IndexRoute;
  var IndexRoute = ReactRouter.IndexRoute;

  var MainPage = React.createClass({
    getInitialState: function() {
      return {};
    },
    componentDidMount: function() {
      $(document).ready(function () {
            $('.dropdown-toggle').dropdown();
        });
      return {};
    },
    render: function() {
      // JSX code
      return  (
          <div id="wrapper">
              <nav className="navbar navbar-inverse navbar-fixed-top" role="navigation">
                  <Navigation />
              </nav>
              <div id="page-wrapper">
                  {this.props.children}
              </div>
          </div>
        );
    }
  });

  return MainPage;

});
