define(['react'], function(React) {
    
    var Dashboard = React.createClass({
        render: function(){
            return(
                <div className="row">
                    <div className="col-lg-12">
                        <h1 className="page-header">
                            This is Dashboard <small>Statistics Overview</small>
                        </h1>
                        <ol className="breadcrumb">
                            <li className="active">
                                <i className="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>
                    </div>
                </div>
            );
        },
    });

    return Dashboard;

});